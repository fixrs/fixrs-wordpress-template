# Fixrs wordpress initialization project

If your distro doesn't come with python, install it :

- Windows :  [python and pip](https://www.python.org/ftp/python/3.8.9/python-3.8.9-embed-amd64.zip)
- Ubuntu/debian : `apt install python3-pip`

Then run the following:

```
pip3 install cookiecutter wget sh
```

Then run :

```
cookiecutter git@bitbucket.org:fixrs/fixrs-wordpress-template.git
```

