from __future__ import print_function
from distutils.dir_util import copy_tree

import zipfile
import wget
import os
import random
import shutil
import string
import fnmatch
from subprocess import run

slug = '{{ cookiecutter.project_slug }}'
slug_dash = '{{ cookiecutter.project_slug }}'.replace('_','-')
slug_upper = '{{ cookiecutter.project_slug }}'.upper()
project_name = '{{ cookiecutter.project_name }}'
        
try:
    random = random.SystemRandom()
    using_sysrandom = True
except NotImplementedError:
    using_sysrandom = False

TERMINATOR = "\x1b[0m"
WARNING = "\x1b[1;33m [WARNING]: "
INFO = "\x1b[1;33m [INFO]: "
HINT = "\x1b[3;33m"
SUCCESS_COLOR = "\x1b[1;32m"
SUCCESS = SUCCESS_COLOR + " [SUCCESS]: "


def download_wordpress():
    os.umask(0)
    url = 'https://wordpress.org/latest.zip'
    wget.download(url, 'wordpress.zip')
    with zipfile.ZipFile('wordpress.zip', 'r') as zip_ref:
        zip_ref.extractall('.')
        shutil.move('wordpress', 'wp')
    os.remove('wordpress.zip')
    os.mkdir('wp/wp-content/languages')
    open('wp/wp-content/languages/.gitkeep', 'a').close()
    os.mkdir('wp/wp-content/languages/plugins/')
    open('wp/wp-content/languages/plugins/.gitkeep', 'a').close()
    os.mkdir('wp/wp-content/uploads')
    os.mkdir('wp/wp-content/cache')


def set_flag(file_path, flag, value=None, formatted=None, *args, **kwargs):
    if value is None:
        random_string = generate_random_string(*args, **kwargs)
        if random_string is None:
            print(
                "We couldn't find a secure pseudo-random number generator on your system. "
                "Please, make sure to manually {} later.".format(flag)
            )
            random_string = flag
        if formatted is not None:
            random_string = formatted.format(random_string)
        value = random_string

    with open(file_path, "r+") as f:
        file_contents = f.read().replace(flag, value)
        f.seek(0)
        f.write(file_contents)
        f.truncate()

    return value


def generate_random_string(
    length=64, using_digits=True, using_ascii_letters=True, using_punctuation=False
):
    if not using_sysrandom:
        return None

    symbols = []
    if using_digits:
        symbols += string.digits
    if using_ascii_letters:
        symbols += string.ascii_letters
    if using_punctuation:
        all_punctuation = set(string.punctuation)
        unsuitable = {"'", '"', "\\", "$"}
        suitable = all_punctuation.difference(unsuitable)
        symbols += "".join(suitable)
    return "".join([random.choice(symbols) for _ in range(length)])


def copy_folder(src, dest):
    copy_tree(src, dest)


def git_init():
    run('git init', shell=True)
    run('git remote add origin git@bitbucket.org:fixrs/' + slug_dash + '.git', shell=True)
    run('git add .', shell=True)
    run('git commit -am"Initial commit"', shell=True)
    print(SUCCESS + "Git initialized" + TERMINATOR)


def wp_config():

    prod_url = input( "What is the URL of the production site? " )
    prod_domain = prod_url.replace('http://','').replace('https://','').strip('/')
    prod_url = 'https://' + prod_domain + '/'
    
    table_prefix = generate_random_string(5,False)
    table_prefix = input(f"Set the table prefix in the current install? ({table_prefix})> ") or table_prefix

    # set table prefix
    sqlCleaner = os.path.join('mariadb-init','Ω-clean.sql')
    set_flag( sqlCleaner, '!!TABLE_PREFIX!!', table_prefix)

    htaccess = os.path.join('.htaccess')
    set_flag( htaccess, '!!PRODUCTION_DOMAIN!!', prod_domain.replace('.','\.') )
    set_flag( htaccess, '!!PRODUCTION_URL!!', prod_url)
    set_flag( htaccess, '!!PROJECT_NAME!!', slug )
    shutil.copy(htaccess, os.path.join('htaccess.backup')) #backup for when we download a full site
    shutil.move(htaccess, os.path.join('wp', '.htaccess'))

    config = os.path.join('wp-config.php')
    set_flag(config, '!!TABLE_PREFIX!!', table_prefix)
    set_flag(config, '!!PRODUCTION_URL!!', prod_url)
    set_flag(config, '!!AUTH_KEY!!')
    set_flag(config, '!!SECURE_AUTH_KEY!!')
    set_flag(config, '!!LOGGED_IN_KEY!!')
    set_flag(config, '!!NONCE_KEY!!')
    set_flag(config, '!!AUTH_SALT!!')
    set_flag(config, '!!SECURE_AUTH_SALT!!')
    set_flag(config, '!!LOGGED_IN_SALT!!')
    set_flag(config, '!!NONCE_SALT!!')
    shutil.copy(config, os.path.join('wp-config-docker.php')) #backup for when we download a full site
    shutil.move(config, os.path.join('wp', 'wp-config.php'))


def download_plugin(plugin_name, plugin_url):
    wget.download(plugin_url, plugin_name + '.zip')
    with zipfile.ZipFile(plugin_name + '.zip', 'r') as zip_ref:
        zip_ref.extractall('.')
    shutil.move(plugin_name, 'wp/wp-content/plugins')
    os.remove(plugin_name + '.zip')


def init_themes_plugins():

    print(INFO + "Init themes and plugins" + TERMINATOR)

    install_newsite = input("Is this a new site? (y|n) > ")
    if 'y' in install_newsite:
        install_acf = input("Install ACF? (y|n) > ")
        install_gravity = input("Install Gravity Forms? (y|n) > ")
        if 'y' in install_gravity:
            install_mailchimp = input("Install Gravity Forms Mailchimp Add-On? (y|n) > ")
        install_wpml = input("Install WPML? (y|n) > ")
        install_yoast = input("Install Yoast SEO? (y|n) > ")
        install_activity_log = input("Install Activity Log? (y|n) > ")
        install_wp_mail_smtp = input("Install WP Mail SMTP? (y|n) > ")
        install_wp_rocket = input("Install WP Rocket? (y|n) > ")
        install_woocommerce = input("Install WooCommerce? (y|n) > ")
    

    install_fixrs_core = input("Install Fixrs Core Plugin? (y|n) > ")
    install_fixrs_theme = input("Prepare and Install the client’s custom theme? (y|n) > ")
    install_fixrs_plugin = input("Prepare and Install the client’s plugin? (y|n) > ")

    acf_url = 'https://connect.advancedcustomfields.com/v2/plugins/download?p=pro&k=b3JkZXJfaWQ9NjY4NDR8dHlwZT1wZXJzb25hbHxkYXRlPTIwMTUtMTAtMTkgMDA6NTQ6MDg='
    woocommerce_url = 'https://downloads.wordpress.org/plugin/woocommerce.zip'
    yoast_url = 'https://downloads.wordpress.org/plugin/wordpress-seo.zip'
    activity_log_url = 'https://downloads.wordpress.org/plugin/aryo-activity-log.zip'
    wp_mail_smtp_url = 'https://downloads.wordpress.org/plugin/wp-mail-smtp.zip'
    wp_rocket_url = 'https://wp-rocket.me/download/62292/b30e34dd/'

    if 'y' in install_newsite:
        
        if 'y' in install_acf:
            download_plugin('advanced-custom-fields-pro', acf_url)

        if 'y' in install_yoast:
            download_plugin('wordpress-seo', yoast_url)

        if 'y' in install_activity_log:
            download_plugin('aryo-activity-log', activity_log_url)

        if 'y' in install_wp_mail_smtp:
            download_plugin('wp-mail-smtp', wp_mail_smtp_url)

        if 'y' in install_wp_rocket:
            download_plugin('wp-rocket', wp_rocket_url)

        if 'y' in install_woocommerce:
            download_plugin('woocommerce', woocommerce_url)

        if 'y' in install_gravity:
            for module in ['gravityforms', 'gravityformsrecaptcha']:
                copy_tree(os.path.join('plugins', module), os.path.join('wp/wp-content/plugins/', module))
            if 'y' in install_mailchimp:
                copy_tree(os.path.join('plugins', 'gravityformsmailchimp'), os.path.join('wp/wp-content/plugins/', 'gravityformsmailchimp'))

        if 'y' in install_wpml:
            for module in ['sitepress-multilingual-cms', 'wpml-string-translation']:
                copy_tree(os.path.join('plugins', module), os.path.join('wp/wp-content/plugins/', module))

        if 'y' in install_wpml and 'y' in install_woocommerce:
            for module in ['woocommerce-multilingual']:
                copy_tree(os.path.join('plugins', module), os.path.join('wp/wp-content/plugins/', module))


    copy_tree(os.path.join('themes'), os.path.join('wp/wp-content/themes'))
    shutil.rmtree(os.path.join('plugins'))
    shutil.rmtree(os.path.join('themes'))

    if 'y' in install_fixrs_core:
        run('git clone git@bitbucket.org:fixrs/fixrs-core.git wp/wp-content/plugins/fixrs-core', shell=True)
        shutil.rmtree(os.path.join('wp/wp-content/plugins/fixrs-core/.git'))
        os.mkdir('wp/wp-content/acf-json')
        open('wp/wp-content/acf-json/.gitkeep', 'a').close()


    if 'y' in install_fixrs_theme:
        run('git clone git@bitbucket.org:fixrs/fixrs-theme.git wp/wp-content/themes/'+slug_dash+'-theme', shell=True)
        shutil.rmtree(os.path.join('wp/wp-content/themes/'+slug_dash+'-theme/.git'))

        find_replace("wp/wp-content/themes/"+slug_dash+"-theme", "{PROJECT_NAME}", project_name, "style.css")
        find_replace("wp/wp-content/themes/"+slug_dash+"-theme", "{fixrs}", slug_dash, "*.php")
        find_replace("wp/wp-content/themes/"+slug_dash+"-theme", "fixrs_", slug+'_', "*.php")
        find_replace("wp/wp-content/themes/"+slug_dash+"-theme", "{fixrs}", slug_dash, "*.po")

        os.rename("wp/wp-content/themes/"+slug_dash+"-theme/assets/images/logo-{fixrs}.svg", "wp/wp-content/themes/"+slug_dash+"-theme/assets/images/logo-"+slug_dash+".svg")

        shutil.rmtree('wp/wp-content/themes/twentytwentytwo', ignore_errors=True)
        shutil.rmtree('wp/wp-content/themes/twentytwentythree', ignore_errors=True)
        shutil.rmtree('wp/wp-content/themes/twentytwentyfour', ignore_errors=True)
        shutil.rmtree('wp/wp-content/themes/twentytwentyfive', ignore_errors=True)


    if 'y' in install_fixrs_plugin:
        run("git clone git@bitbucket.org:fixrs/fixrs-plugin.git wp/wp-content/plugins/"+slug_dash+"-plugin", shell=True)
        shutil.rmtree(os.path.join('wp/wp-content/plugins/'+slug_dash+'-plugin/.git'))

        os.rename("wp/wp-content/plugins/"+slug_dash+"-plugin/fixrs-plugin.php", "wp/wp-content/plugins/"+slug_dash+"-plugin/"+slug_dash+"-plugin.php")
        os.rename("wp/wp-content/plugins/"+slug_dash+"-plugin/languages/{fixrs}-plugin.pot", "wp/wp-content/plugins/"+slug_dash+"-plugin/languages/"+slug_dash+"-plugin.pot")

        find_replace("wp/wp-content/plugins/"+slug_dash+"-plugin", "{PROJECT_NAME}", project_name,"*.php")
        find_replace("wp/wp-content/plugins/"+slug_dash+"-plugin", "{fixrs}", slug_dash,"*.php")
        find_replace("wp/wp-content/plugins/"+slug_dash+"-plugin", "FIXRS", slug_upper,"*.php")
        find_replace("wp/wp-content/plugins/"+slug_dash+"-plugin", "{fixrs}", slug_dash,"*.po")
        find_replace("wp/wp-content/plugins/"+slug_dash+"-plugin", "{fixrs}", slug_dash,"*.pot")

    shutil.rmtree('wp/wp-content/plugins/akismet/')
    os.remove('wp/wp-content/plugins/hello.php')




def find_replace(directory, find, replace, filePattern):
    for path, dirs, files in os.walk(os.path.abspath(directory)):
        for filename in fnmatch.filter(files, filePattern):
            filepath = os.path.join(path, filename)
            with open(filepath) as f:
                s = f.read()
            s = s.replace(find, replace)
            with open(filepath, "w") as f:
                f.write(s)

def docker_setup():
    print(INFO + "Validating you have the fixrs docker network" + TERMINATOR)
    networks = run("docker network ls", capture_output=True, shell=True)
    if 'fixrs' not in str(networks.stdout):
        print(INFO + "Creating fixrs docker network" + TERMINATOR)
        run('docker network create fixrs', shell=True)
    else:
        print(INFO + "fixrs docker network found" + TERMINATOR)
	

def download_utils():
    print(INFO + "Validating you have the fixrs utils package" + TERMINATOR)
    if os.path.isdir(os.path.join('../', 'fixrs-utils')) is False:
        print(INFO + "Could not find utils, downloading" + TERMINATOR)
        run('git clone -b dev git@bitbucket.org:fixrs/traefik.git ../fixrs-utils', shell=True)
        print(SUCCESS + "Fixrs utils downloaded" + TERMINATOR)
    else:
        print(SUCCESS + "Fixrs utils found" + TERMINATOR)


def main():
    print(INFO + "Downloading wordpress" + TERMINATOR)
    download_wordpress()
    print(TERMINATOR + SUCCESS + "Wordpress downloaded" + TERMINATOR)
    print(INFO + "Generating salt values" + TERMINATOR)
    wp_config()
    
    init_themes_plugins()
    git_init()
    docker_setup()
    download_utils()
    
    print(SUCCESS + "Project initialized, keep up the good work!" + TERMINATOR)
    
    print(INFO + "Go to https://bitbucket.org/repo/create?owner=fixrs to generate a new project with the name " + SUCCESS_COLOR + " " + slug_dash + " " + TERMINATOR)
    print(INFO + "https://bitbucket.org/fixrs/" + slug_dash + "/admin/addon/admin/pipelines/settings to activate pipelines" + TERMINATOR)
    print(SUCCESS + "--------------------------" + TERMINATOR)
    print(INFO + "Make sure to start fixrs-utils : cd fixrs-utils && docker-compose up -d " + TERMINATOR)
    print(INFO + "Then you can start your own project : cd {{ cookiecutter.project_slug }}" + TERMINATOR)


if __name__ == "__main__":
    main()
