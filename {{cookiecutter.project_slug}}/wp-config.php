<?php

if (file_exists( __DIR__ .'/wp-config-local.php')) {
    // create wp-config-local.php if on a server different than local or production
    require_once( __DIR__ .'/wp-config-local.php');
}else{
	// update PROD_ values with values from the production wp-config.php
    define('DB_NAME',          getenv('DB_NAME')     ?: '!!PROD_DB_NAME!!');
    define('DB_USER',          getenv('DB_USER')     ?: '!!PROD_DB_USER!!');
    define('DB_PASSWORD',      getenv('DB_PASSWORD') ?: '!!PROD_DB_PASSWORD!!');
    define('DB_HOST',          getenv('DB_HOST')     ?: '!!PROD_DB_HOST!!');
    define('WP_HOME',          getenv('WP_HOME')     ?: '!!PRODUCTION_URL!!');
}

define('DB_CHARSET',       'utf8mb4');
define('DB_COLLATE',       '');

// update SALTs with values from the production wp-config.php
define('AUTH_KEY',         '!!AUTH_KEY!!');
define('SECURE_AUTH_KEY',  '!!SECURE_AUTH_KEY!!');
define('LOGGED_IN_KEY',    '!!LOGGED_IN_KEY!!');
define('NONCE_KEY',        '!!NONCE_KEY!!');
define('AUTH_SALT',        '!!AUTH_SALT!!');
define('SECURE_AUTH_SALT', '!!SECURE_AUTH_SALT!!');
define('LOGGED_IN_SALT',   '!!LOGGED_IN_SALT!!');
define('NONCE_SALT',       '!!NONCE_SALT!!');

define('WP_SITEURL',        WP_HOME);


// Config specific to production
if ( WP_HOME === '!!PRODUCTION_URL!!' ) {

}

$table_prefix = '!!TABLE_PREFIX!!_';

$do_debug = (defined(constant_name: 'WP_CLI') && WP_CLI) ? false : (strpos(WP_HOME, 'localhost') !== false || isset($_COOKIE['DEBUG']));

define('WP_DEBUG', $do_debug );
define('WP_DEBUG_DISPLAY', isset( $_COOKIE['DEBUG'] ) );
define('WP_DEBUG_LOG', $do_debug);

define('RECOVERY_MODE_EMAIL', 'errors@fixrs.ca' );
define('AUTOMATIC_UPDATER_DISABLED', true );
define('DISALLOW_FILE_MODS',!$do_debug);


if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
  $_SERVER['HTTPS'] = 'on';
}
if (!defined('ABSPATH' ) ) {
  define('ABSPATH', dirname( __FILE__ ) . '/' );
}

require_once( ABSPATH . 'wp-settings.php' );

