DELIMITER //
CREATE FUNCTION IS_EMAIL(email VARCHAR(255)) 
RETURNS BOOLEAN 
DETERMINISTIC
BEGIN
    RETURN email REGEXP '^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$';
END //

DELIMITER ;

-- General tables

UPDATE !!TABLE_PREFIX!!_users SET user_email = CONCAT('user+',ID,'@fixrs.ca') WHERE user_email NOT LIKE '%@fixrs.ca';

UPDATE !!TABLE_PREFIX!!_usermeta SET meta_value = CONCAT('user+',user_id,'@fixrs.ca')  WHERE IS_EMAIL(meta_value) && meta_value NOT LIKE '%@fixrs.ca';

UPDATE !!TABLE_PREFIX!!_postmeta SET meta_value = CONCAT('post+',post_id,'@fixrs.ca')  WHERE IS_EMAIL(meta_value) && meta_value NOT LIKE '%@fixrs.ca';

UPDATE !!TABLE_PREFIX!!_comments SET comment_author_email = CONCAT('comment+',ID,'@fixrs.ca') WHERE comment_author_email NOT LIKE '%@fixrs.ca';

-- WooCommerce is installed

UPDATE !!TABLE_PREFIX!!_ordermeta SET meta_value = CONCAT('order+',order_id,'@fixrs.ca')  WHERE IS_EMAIL(meta_value) && meta_value NOT LIKE '%@fixrs.ca';

UPDATE !!TABLE_PREFIX!!_wc_order_addresses SET email = CONCAT('order+',order_id,'@fixrs.ca')  WHERE email NOT LIKE '%@fixrs.ca';


DROP FUNCTION IF EXISTS IS_EMAIL;
