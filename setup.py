#!/usr/bin/env python

import os
import sys

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

version = "0.1.0"

if sys.argv[-1] == "tag":
    os.system(f'git tag -a {version} -m "version {version}"')
    os.system("git push --tags")
    sys.exit()

with open("README.rst") as readme_file:
    long_description = readme_file.read()

setup(
    name="cookiecutter-wordpress",
    version=version,
    description="A Cookiecutter template for creating production-ready WordPress projects quickly.",
    long_description=long_description,
    author="Pierre Paul Lefebvre",
    author_email="pierrepaul@fixrs.ca",
    url="https://fixrs.ca",
    packages=[],
    license="BSD",
    zip_safe=False,
    classifiers=[
        "Development Status :: 0.1.0 - Beta",
        "Environment :: Console",
        "Intended Audience :: Developers",
        "Natural Language :: English",
        "License :: OSI Approved :: BSD License",
        "Programming Language :: PHP",
        "Topic :: Software Development",
    ],
    keywords=(
        "cookiecutter, wordpress, projects, project templates, django, "
        "skeleton, scaffolding, project directory"
    ),
)
